$(document).ready(function() {
    initSlider('.my-slider');
});

function initSlider(element){
    var slider = tns({
        container: element,
        items: 3,    
        gutter: 3, 
        controlsContainer: '#customize-controls',
        navContainer: '#dots',
        responsive: {
            500:{
                items: 5
            }
        },
        onInit: function(){
            $('.tns-outer').addClass('order-2');
        }
      });
}
